require(['jquery', 'jquery/ui'],function($){
    $(document).on("click","table.table-checkout-shipping-method tr", function() {
        $('#dpd-pickup-point-select').change(function(){ 
            var selectedText = $(this).find("option:selected").text();
            selectedText = selectedText.split(',');
            var street = selectedText[1].trim();
            var otherText = selectedText[2].trim().split(' ');
            var city = otherText[0].trim();
            var postCode = otherText[1].trim();

            setAddress(street, city, postCode);

            jQuery('#co-shipping-form fieldset.field.street').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.city"]').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.postcode"]').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.country_id"]').hide();
        });

        $carrierId = $(this).find('.col-carrier').attr('id');

        if ($carrierId == 'label_carrier_freeshipping_freeshipping') {
            var street = 'Vytauto g. 100';
            var city = 'Palanga';
            var postCode = '00132';

            setAddress(street, city, postCode);

            jQuery('#co-shipping-form fieldset.field.street').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.city"]').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.postcode"]').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.country_id"]').hide();
            jQuery('#co-shipping-form div.opc-payment-additional.comment').hide();
        }

        if ($carrierId == 'label_carrier_pickup_dpd') {
            $('#dpd-pickup-point-select').change();

            jQuery('#co-shipping-form fieldset.field.street').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.city"]').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.postcode"]').hide();
            jQuery('#co-shipping-form div[name="shippingAddress.country_id"]').hide();
        }

        if ($carrierId == 'label_carrier_classic_dpd') {
            jQuery('#co-shipping-form fieldset.field.street').show();
            jQuery('#co-shipping-form div[name="shippingAddress.city"]').show();
            jQuery('#co-shipping-form div[name="shippingAddress.postcode"]').show();
            jQuery('#co-shipping-form div[name="shippingAddress.country_id"]').show();
            jQuery('#co-shipping-form div.opc-payment-additional.comment').show();
            jQuery('#co-shipping-form div[name="shippingAddress.street.0"] input').val('').keyup();
            jQuery('#co-shipping-form div[name="shippingAddress.city"] input').val('').keyup();
            jQuery('#co-shipping-form div[name="shippingAddress.postcode"] input').val('').keyup();
        }

    });

    function setAddress(street, city, postCode) {
        jQuery('#co-shipping-form div[name="shippingAddress.street.0"] input').val(street).keyup();
        jQuery('#co-shipping-form div[name="shippingAddress.city"] input').val(city).keyup();
        jQuery('#co-shipping-form div[name="shippingAddress.postcode"] input').val(postCode).keyup();
    }
});