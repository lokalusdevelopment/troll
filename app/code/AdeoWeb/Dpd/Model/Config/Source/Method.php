<?php

namespace AdeoWeb\Dpd\Model\Config\Source;

/**
 * Class Method
 */
class Method extends Generic
{
    const TYPE = 'method';

    /**
     * @var string
     */
    protected $code = self::TYPE;
}
