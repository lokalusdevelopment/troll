<?php

namespace AdeoWeb\Dpd\Model\Config\Source;

/**
 * Class AvailableTimes
 * @codeCoverageIgnore
 */
class AvailableTimes extends Generic
{
    const TYPE = 'available_times';

    /**
     * @var string
     */
    protected $code = self::TYPE;
}
