<?php

namespace Paysera\Magento2Paysera\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use WebToPay;
use Exception;
use Magento\Framework\Controller\Result\Raw;

class Callback extends \Magento\Framework\App\Action\Action
{
    const PAYSERA_PAYMENT = 'payment/paysera';

    protected $_scopeConfig;
    protected $_orderRepository;
    protected $_rawResult;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        OrderRepositoryInterface $orderRepository,
        Raw $rawResult
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_orderRepository = $orderRepository;
        $this->_rawResult = $rawResult;

        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     *
     * @throws Exception
     */
    public function execute()
    {
        $paysera_config = $this->_scopeConfig->getValue(
            self::PAYSERA_PAYMENT,
            ScopeInterface::SCOPE_STORE
        );

        switch (filter_input(INPUT_SERVER, 'REQUEST_METHOD')) {
            case 'GET':
                $requestData = filter_input_array(INPUT_GET);
                break;
            default:
                $requestData = filter_input_array(INPUT_POST);
                break;
        }

        try {
            $response = WebToPay::validateAndParseData(
                $requestData,
                $paysera_config['projectid'],
                $paysera_config['sign_password']
            );

            if ($response['status'] == 1) {
                $order = $this->_orderRepository->get($response['orderid']);

                $orderTotalCents = $order->getGrandTotal() * 100;

                $money = [
                    'amount' => (string)$orderTotalCents,
                    'currency' => $order->getOrderCurrencyCode()
                ];

                $isPaymentCorrect = $this->checkPayment($money, $response);

                if ($isPaymentCorrect) {
                    $order->setStatus(
                        $paysera_config['paysera_order_status']['order_status']
                    )->save();

                    return $this->_rawResult->setContents('OK');
                }
            }
        } catch (Exception $e) {
            return $this->_rawResult->setContents(get_class($e) . ': ' . $e->getMessage());
        }
    }

    /**
     * @param array $orderMoney
     * @param array $response
     *
     * @return bool
     *
     * @throws LocalizedException
     */
    public function checkPayment($orderMoney, $response)
    {
        $checkConvert = array_key_exists('payamount', $response);
        $orderAmount = $orderMoney['amount'];
        $orderCurrency = $orderMoney['currency'];

        if ($response['amount'] !== $orderAmount || ($checkConvert && $response['payamount'] !== $orderAmount)) {
            throw new LocalizedException(__('Amounts do not match'));
        }

        if ($response['currency'] !== $orderCurrency
            || ($checkConvert && $response['paycurrency'] !== $orderCurrency)
        ) {
            throw new LocalizedException(__('Currencies do not match'));
        }

        return true;
    }
}
