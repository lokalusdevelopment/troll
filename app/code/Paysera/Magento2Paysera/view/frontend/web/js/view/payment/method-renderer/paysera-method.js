/*browser:true*/
/*global define*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment/additional-validators',
    ],
    function (
        $,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        checkoutData,
        additionalValidators
    ) {
        'use strict';

        let countryId, selectedCountryId, paymentId, data;

        let paymentMethods = {
            /**
             * @return {object}
             */
            updateSelection: function () {
                selectedCountryId = checkoutData.getShippingAddressFromData().country_id;

                let payseraCountriesOption = $('#paysera_country option').length > 0 ?
                    $('#paysera_country option') : $('.payment-countries').attr('id');

                let otherCountryId = "other";

                let possibleCountries = $.map(payseraCountriesOption, function (option) {
                    return option.value;
                });

                let selectedCountry = selectedCountryId.toLowerCase();

                countryId = $.inArray(selectedCountry, possibleCountries) === -1 ?
                    ($.inArray(otherCountryId, possibleCountries) === -1 ? possibleCountries[0] : otherCountryId) :
                    selectedCountry
                ;

                $('#paysera_country option[value="' + countryId + '"]').prop('selected', true);
                $('.payment-countries').hide();
                $('#' + countryId).show('slow');
            },
        };

        $(window).bind('hashchange', paymentMethods.updateSelection);

        $(document).on('change', '#paysera_country', function () {
            countryId = $('#paysera_country').val();
            $('.payment-countries').hide();
            $('#' + countryId).show('slow');
        });

        $(document).on('click', '.payment', function () {
            paymentId = this.id;
        });

        $(document).ready(function () {
            let existCondition = setInterval(function () {
                if ($('.payment-method').length) {
                    clearInterval(existCondition);
                    paymentMethods.updateSelection();
                }
            }, 100);
        });

        return Component.extend({
            defaults: {
                template: 'Paysera_Magento2Paysera/paysera',
            },

            getTitle: function () {
                return window.checkoutConfig.payment.paysera.title;
            },

            getData: function () {
                data = {
                    'method': this.getCode(),
                    'additional_data': {
                        'paysera_payment_method': paymentId
                    }
                };

                return data;
            },

            placeOrder: function (data, event) {
                if (event) {
                    event.preventDefault();
                }

                let self = this,
                    placeOrder,
                    emailValidationResult = customer.isLoggedIn(),
                    loginFormSelector = 'form[data-role=email-with-possible-login]';

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (emailValidationResult && this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                    $.when(placeOrder).fail(function () {
                        self.isPlaceOrderActionAllowed(true);
                    }).done(this.afterPlaceOrder.bind(this));

                    return true;
                }

                return false;
            },

            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);

                return true;
            },

            afterPlaceOrder: function () {
                let xmlhttp = new XMLHttpRequest();

                let url = window.checkoutConfig.payment.paysera.pageBaseUrl + "/paysera";

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        let response = JSON.parse(xmlhttp.responseText);
                        window.location.href = response['url'];
                    }
                };
                xmlhttp.open("GET", url, true);
                xmlhttp.send();
            },

            getPaymentMarkSrc: function () {
                return window.checkoutConfig.payment.paysera.logo;
            },

            getCountriesSelection: function () {
                let paymentMethods = window.checkoutConfig.payment.paysera.countries;

                paymentMethods.replace(/display:block/g, "display:none");

                return paymentMethods;
            }
        });
    }
);
