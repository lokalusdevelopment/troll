plugin_magento_2
=======================

Version: 3.0.6

Contributors: Paysera

Tags: online payment, payment, payment gateway, SMS payments, international payments, mobile payment, sms pay, payment by sms, billing system, payment institution, macro payments, micro payments, sms bank

Requires at least: 2.1

Tested up to: 2.3.5

Requirements: Magento

License: GPLv3

License URL: http://www.gnu.org/licenses/gpl-3.0.html


Description
-----------

Collecting payments with Paysera is simple, fast and secure. It is enough to open Paysera account, install the plug-in to your store and you will be able to use all of the most popular ways of payment collection in one place.
No need for complicated programming or integration work. With Paysera you will receive real time notifications about successful transactions in your online store, and the money will reach you sooner.
No monthly, registration or connection fee.

It is simple to use and administer payment collection, and you can monitor movement of funds in your smartphone.
Client services and consultation takes place 7 days a week, from 8:00 till 20:00 (EET)
Payments are made in real time.

Paysera applies the lowest fees on the market, and payments from foreign banks and systems are converted at best possible rates.

To use this plugin, register at paysera.com and create your project. You will get your project ID and signature, which should be written in this plugin settings.


Installation
------------	

Video: https://youtu.be/2EJhTjE7TZg

-= Installation by FTP =-

1. Download Paysera module zip file;

2. Connect to the FTP server and go to Magento base directory;

3. Go to folder:
    /app/code
    
    If folder "code" doesn't exist - create it;
    
4. Create folder "Paysera" and open it;

5. Create folder "Magento2Paysera" and open it;

6. Extract the content of Paysera plugin zip file;

7. Open the Terminal and go to the base directory of Magento and enter these commands:

    composer require "webtopay/libwebtopay":"1.6.*"
    
    php bin/magento setup:upgrade
    
    php bin/magento setup:di:compile
     
8. Connect to Magento Admin panel.

9. Configure Paysera module in Magento Admin panel:
    Stores - Configuration - Sales - Payment Methods - Paysera
    
10. Save changes.


-= Installation using marketplace =-

1. Connect to Magento Marketplace and search for Paysera Payment Gateway;

2. Order Paysera plugin;

3. Press Install after the order is complete;

4. Open the Terminal and go to the base directory of Magento and enter these commands:

    composer require "payserauk/magento2-paysera-module":"3.0.*"
    
    php bin/magento setup:upgrade
    
    php bin/magento setup:di:compile
     
5. Connect to Magento Admin panel.

6. Configure Paysera module in Magento Admin panel:
    Stores - Configuration - Sales - Payment Methods - Paysera
    
7. Save changes.


Version history
---------------
Version 3.0.6   - 2020-06-10
    
    * Code style fixes

Version 3.0.5   - 2020-05-14
    
    * Added payment information title
    * Improved country selection logic

Version 3.0.4   - 2020-04-01
    
    * Code fixes

Version 3.0.3   - 2019-03-22

    * Code fixes
    * Code improvement
    * Added language parameter
    * Added translations
    * Doc Blocks update
    
Version 3.0.2   - 2018-11-08

    * Code fixes

Version 3.0.1   - 2017-10-23

    * Code fixes

Version 3.0.0   - 2017-10-16

    * Initial release


Support
-------

For any questions, please look for the answers at https://support.paysera.com or contact customer support center by email  support@paysera.com or by phone +44 20 80996963 | +370 700 17217.

For technical documentation visit: https://developers.paysera.com/
