<?php

namespace Paysera\Magento2Paysera\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Paysera\Magento2Paysera\Helper\Data;
use WebToPay;

class PayseraConfigProvider implements ConfigProviderInterface
{
    const EMPTY_CODE = '';
    const DELIMITER = ',';

    const PROJECT_ID = 'projectid';
    const EXTRA_CONF = 'paysera_extra';
    const PAY_TITLE = 'title';
    const PAY_DESCRIPTION = 'description';
    const PAY_ALL_COUNTRIES = 'allowspecific';
    const PAY_SELECT_COUNTRIES = 'specificcountry';
    const PAY_GRID = 'grid';
    const PAY_LIST = 'payment_list';
    const REDIRECT = 'payseraRedirectUrl';

    const DEFAULT_PROJECT_ID = 0;

    const COUNTRY_SELECT_MIN = 1;
    const LOGO_URL = 'https://www.paysera.lt/f/logotipas_internetui.png';
    const LINE_BREAK = '<div style="clear:both"><br /></div>';

    protected $_helper;

    public function __construct(Data $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @return array
     *
     * @throws \WebToPayException|\Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                'paysera' => [
                    'title' => $this->getPluginConfig(
                        self::EXTRA_CONF,
                        self::PAY_TITLE
                    ),
                    'logo' => self::LOGO_URL,
                    'countries' => $this->getPayseraPayments(),
                    'pageBaseUrl' => $this->getPageUrl(),
                ]
            ]
        ];

        return $config;
    }

    /**
     * @return string
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getPageUrl()
    {
        $url = $this->_helper->getPageBaseUrl();

        return $url;
    }

    /**
     * @return int
     */
    protected function getOrderAmmount()
    {
        $total = $this->_helper->getTotalAmmount() * 100;

        return $total;
    }

    /**
     * @return string
     */
    protected function getOrderCurrency()
    {
        $currencyCode = $this->_helper->getOrderCurrencyCode();

        return $currencyCode;
    }

    /**
     * @return string
     */
    protected function getOrderCountryCode()
    {
        $countryCode = $this->_helper->getOrderCountryCode();

        return $countryCode;
    }

    /**
     * @return string
     */
    protected function getStoreLang()
    {
        $languageCode = $this->_helper->getStoreLangCode();

        return $languageCode;
    }

    /**
     * @param string $group
     * @param string $name
     *
     * @return string
     */
    protected function getPluginConfig($group, $name = null)
    {
        return $this->_helper->getPluginConfig($group, $name);
    }

    /**
     * @param string $project
     * @param string $currency
     * @param string $lang
     *
     * @return \WebToPay_PaymentMethodCountry[]
     *
     * @throws \WebToPayException
     */
    protected function getPayseraCountries($project, $currency, $lang)
    {
        $countries = WebToPay::getPaymentMethodList(
            $project,
            $currency
        )->setDefaultLanguage(
            $lang
        )->getCountries();

        return $countries;
    }

    /**
     * @param array $countries
     *
     * @return array
     */
    public function getCountriesList($countries)
    {
        $countriesList = [];

        $showSelectedCountries = $this->getPluginConfig(
            self::EXTRA_CONF,
            self::PAY_ALL_COUNTRIES
        );

        $selectedCountriesList = $this->getPluginConfig(
            self::EXTRA_CONF,
            self::PAY_SELECT_COUNTRIES
        );

        $selectedCountriesCodes = explode(
            self::DELIMITER,
            $selectedCountriesList
        );

        foreach ($countries as $country) {
            if (!(bool)$showSelectedCountries
                || in_array($country->getCode(), $selectedCountriesCodes)
            ) {
                $countriesList[] = [
                    'code' => $country->getCode(),
                    'title' => $country->getTitle(),
                    'groups' => $country->getGroups()
                ];
            }
        }

        return $countriesList;
    }

    /**
     * @return string
     *
     * @throws \WebToPayException
     */
    protected function getPayseraPayments()
    {
        $buildHtml = new BuildHtmlCode();

        $billingCountryCode = $this->getOrderCountryCode();

        $displayGridView = $this->getPluginConfig(
            self::EXTRA_CONF,
            self::PAY_GRID
        );

        $showInPage = $this->getPluginConfig(
            self::EXTRA_CONF,
            self::PAY_LIST
        );

        if ((bool)$showInPage) {
            $configProjectId = $this->getPluginConfig(self::PROJECT_ID);

            $payseraCountries = $this->getPayseraCountries(
                $this->getValidProject($configProjectId),
                $this->getOrderCurrency(),
                $this->getStoreLang()
            );

            $countries = $this->getCountriesList($payseraCountries);

            if (count($countries) > self::COUNTRY_SELECT_MIN) {
                $paymentsHtml = $buildHtml->buildCountriesList(
                    $countries,
                    $billingCountryCode
                );
                $paymentsHtml .= self::LINE_BREAK;
            } else {
                $paymentsHtml = self::EMPTY_CODE;
            }

            $paymentsHtml .= $buildHtml->buildPaymentsList(
                $countries,
                $displayGridView,
                $billingCountryCode
            );
            $paymentsHtml .= self::LINE_BREAK;
        } else {
            $paymentsHtml = $this->getPluginConfig(
                self::EXTRA_CONF,
                self::PAY_DESCRIPTION
            );
        }

        return $paymentsHtml;
    }

    /**
     * @param string $projectID
     *
     * @return string
     */
    protected function getValidProject($projectID)
    {
        if (filter_var($projectID, FILTER_VALIDATE_INT) !== false) {
            $definedProjectID = $projectID;
        } else {
            $definedProjectID = $this::DEFAULT_PROJECT_ID;
        }

        return $definedProjectID;
    }
}
