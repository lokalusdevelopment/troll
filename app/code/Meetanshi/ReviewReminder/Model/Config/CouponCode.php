<?php

namespace Meetanshi\ReviewReminder\Model\Config;

use Magento\Framework\Option\ArrayInterface;
use Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory;

/**
 * Class CouponCode
 * @package Meetanshi\ReviewReminder\Model\Config
 */
class CouponCode implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    private $ruleFactory;

    /**
     * CouponCode constructor.
     * @param CollectionFactory $ruleFactory
     */
    public function __construct(CollectionFactory $ruleFactory)
    {
        $this->ruleFactory = $ruleFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {

        $optionArray = array(["value" => '0', "label" => __('No')]);

        $couponCollection = $this->ruleFactory->create();
        $couponCollection->addFieldToFilter('is_active', 1);

        foreach ($couponCollection as $rule) {
            $optionArray[] = ["value" => $rule->getId(), "label" => __($rule->getName())];
        }

        return $optionArray;

    }
}
