<?php

namespace Meetanshi\ReviewReminder\Model\ResourceModel\Reminder;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Meetanshi\ReviewReminder\Model\ResourceModel\Reminder
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'reminder_id';
    /**
     * @var string
     */
    protected $_eventPrefix = 'meetanshi_review_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'meetanshi_review_collection';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Meetanshi\ReviewReminder\Model\Reminder', 'Meetanshi\ReviewReminder\Model\ResourceModel\Reminder');
    }
}