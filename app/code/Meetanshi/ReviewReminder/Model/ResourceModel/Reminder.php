<?php


namespace Meetanshi\ReviewReminder\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;


/**
 * Class Reminder
 * @package Meetanshi\ReviewReminder\Model\ResourceModel
 */
class Reminder extends AbstractDb
{
    /**
     * @var string
     */
    protected $_idFieldName = 'reminder_id';
    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * Reminder constructor.
     * @param Context $context
     * @param DateTime $date
     * @param null $resourcePrefix
     */
    public function __construct(
        Context $context,
        DateTime $date,
        $resourcePrefix = null
    )
    {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('meetanshi_review_reminder', 'reminder_id');
    }
}
