<?php

namespace Meetanshi\ReviewReminder\Model;

use Meetanshi\ReviewReminder\Api\Data\ReminderInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Reminder
 * @package Meetanshi\ReviewReminder\Model
 */
class Reminder extends AbstractModel implements ReminderInterface
{
    /**
     *
     */
    const CACHE_TAG = 'meetanshi_review_reminder';

    /**
     * @var string
     */
    protected $_cacheTag = 'meetanshi_review_reminder';

    /**
     * @var string
     */
    protected $_eventPrefix = 'meetanshi_review_reminder';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('Meetanshi\ReviewReminder\Model\ResourceModel\Reminder');
    }

    /**
     * @return mixed
     */
    public function getReminderId()
    {
        return $this->getData(self::REMINDER_ID);
    }

    /**
     * @param $reminderId
     * @return Reminder|mixed
     */
    public function setReminderId($reminderId)
    {
        return $this->setData(self::REMINDER_ID, $reminderId);
    }

    /**
     * @return mixed
     */
    public function getMailStatus()
    {
        return $this->getData(self::MAIL_STATUS);
    }

    /**
     * @param $mailStatus
     * @return Reminder|mixed
     */
    public function setMailStatus($mailStatus)
    {
        return $this->setData(self::MAIL_STATUS, $mailStatus);
    }

    /**
     * @return mixed
     */
    public function getSentCount()
    {
        return $this->getData(self::SENT_COUNT);
    }

    /**
     * @param $sentCount
     * @return Reminder|mixed
     */
    public function setSentCount($sentCount)
    {
        return $this->setData(self::SENT_COUNT, $sentCount);
    }

    /**
     * @return mixed
     */
    public function getIncrementId()
    {
        return $this->getData(self::INCREMENT_ID);
    }

    /**
     * @param $incrementId
     * @return Reminder|mixed
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->getData(self::MAIL);
    }

    /**
     * @param $mail
     * @return Reminder|mixed
     */
    public function setMail($mail)
    {
        return $this->setData(self::MAIL, $mail);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param $status
     * @return Reminder|mixed
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @param $createdAt
     * @return Reminder|mixed
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
