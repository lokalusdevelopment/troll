<?php

namespace Meetanshi\ReviewReminder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Meetanshi\ReviewReminder\Helper\Data;
use Magento\Framework\Registry;
use Meetanshi\ReviewReminder\Model\ResourceModel\Reminder\Grid\CollectionFactory;
use Meetanshi\ReviewReminder\Model\Reminder as ReminderFactory;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\SalesRule\Model\RuleFactory;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\App\ResponseFactory;

/**
 * Class Review
 * @package Meetanshi\ReviewReminder\Observer
 */
class Review implements ObserverInterface
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var CollectionFactory
     */
    private $reminderFactory;
    /**
     * @var RedirectInterface
     */
    private $request;
    /**
     * @var ReminderFactory
     */
    private $reminderModel;
    /**
     * @var RuleFactory
     */
    private $rule;
    /**
     * @var PriceHelper
     */
    private $priceHelper;
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * Review constructor.
     * @param Data $helper
     * @param Registry $registry
     * @param RedirectInterface $request
     * @param CollectionFactory $reminderFactory
     * @param ReminderFactory $reminderModel
     * @param ResponseFactory $responseFactory
     * @param RuleFactory $rule
     * @param PriceHelper $priceHelper
     */
    public function __construct(
        Data $helper,
        Registry $registry,
        RedirectInterface $request,
        CollectionFactory $reminderFactory,
        ReminderFactory $reminderModel,
        ResponseFactory $responseFactory,
        RuleFactory $rule,
        PriceHelper $priceHelper
    )
    {
        $this->helper = $helper;
        $this->request = $request;
        $this->registry = $registry;
        $this->reminderFactory = $reminderFactory;
        $this->reminderModel = $reminderModel;
        $this->rule = $rule;
        $this->priceHelper = $priceHelper;
        $this->responseFactory = $responseFactory;
    }

    /**
     * @param Observer $observer
     * @return $this|string|void
     */
    public function execute(Observer $observer)
    {
        if ($config = $this->helper->getConfig()) {
            try {
                $url = $this->request->getRefererUrl();
                $redirectionUrl = parse_url($url);
                $url = parse_url($url, PHP_URL_QUERY);
                parse_str($url, $order);
                if (isset($order['order'])) {
                    $product = $this->registry->registry("current_product");

                    $reminders = $this->reminderFactory->create()
                        ->addFieldToFilter('increment_id', array('eq' => $order['order']))
                        ->addFieldToFilter('product_id', array('notnull' => true));
                    $conter = $reminders->count();

                    if ($conter) {
                        $config = array();
                        $reminderModel = $this->reminderModel->load($reminders->getFirstItem()->getData('reminder_id'));
                        $productsId = explode(',', $reminderModel->getData('product_id'));

                        if (in_array($product->getId(), $productsId)) {
                            $pos = array_search($product->getId(), $productsId);
                            unset($productsId[$pos]);
                            $reminderModel->setData('mail_status', 'sent');
                            if (empty(implode(',', $productsId))) {
                                $reminderModel->setData('status', 'submitted');
                                if ($this->helper->getCartRule($reminderModel->getData('store_id'))) {
                                    $couponCodeData = $this->rule->create()->load($this->helper->getCartRule($reminderModel->getData('store_id')));
                                    if ($couponCodeData->getSimpleAction() == 'by_percent') {
                                        $config['discount'] = number_format($couponCodeData->getDiscountAmount(), 2) . '%';
                                    } elseif ($couponCodeData->getSimpleAction() == 'by_fixed') {
                                        $config['discount'] = $this->priceHelper->currency($couponCodeData->getDiscountAmount(), true, false);
                                    }
                                    if ($couponCodeData->getUseAutoGeneration()) {
                                        $coupons = $couponCodeData->getCoupons();

                                        foreach ($coupons as $coupon) {
                                            if ($coupon->getTimesUsed() <= $coupon->getUsageLimit()) {
                                                $config['coupon'] = $coupon->getCode();
                                                break;
                                            }
                                        }
                                    } else {
                                        $config['coupon'] = $couponCodeData->getCouponCode();
                                    }
                                    $this->helper->sendCouponCodeMail($reminderModel, $config);
                                }
                            }
                            $reminderModel->setData('product_id', implode(',', $productsId));
                            $reminderModel = $this->helper->saveReminder($reminderModel);
                        }
                    }
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }
        return $this;
    }

}
