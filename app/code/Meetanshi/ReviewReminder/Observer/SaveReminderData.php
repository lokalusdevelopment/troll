<?php


namespace Meetanshi\ReviewReminder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Meetanshi\ReviewReminder\Model\ReminderFactory;
use Magento\Sales\Model\Order;
use Meetanshi\ReviewReminder\Helper\Data;

/**
 * Class SaveReminderData
 * @package Meetanshi\ReviewReminder\Observer
 */
class SaveReminderData implements ObserverInterface
{
    /**
     * @var ReminderFactory
     */
    private $reminderFactory;
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Data
     */
    private $helper;

    /**
     * SaveReminderData constructor.
     * @param ReminderFactory $reminderFactory
     * @param Order $order
     * @param Data $helper
     */
    public function __construct(
        ReminderFactory $reminderFactory,
        Order $order,
        Data $helper
    )
    {
        $this->reminderFactory = $reminderFactory;
        $this->order = $order;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     * @return string|void
     */
    public function execute(Observer $observer)
    {
        try {
            $orderId = $observer->getEvent()->getOrderIds();
            $order = $this->order->load($orderId[0]);
            $storeId = $order->getStoreId();

            if ($this->helper->getConfig($storeId)) {
                $reminderModel = $this->reminderFactory->create();
                $reminderModel->setMailStatus('pending');
                $reminderModel->setStatus('pending');
                $product = array();
                foreach ($order->getAllVisibleItems() as $item) {
                    if ($item->getParentItemId() == '') {
                        $product[] = $item->getProductId();
                    }
                }
                $product = implode(',', $product);
                $reminderModel->setProductId($product);
                $reminderModel->setMail($order->getCustomerEmail());
                $reminderModel->setIncrementId($order->getIncrementId());
                $reminderModel->setStoreId($storeId);

                if ($this->helper->getDays($storeId) == 0) {
                    $customerGroup = explode(',', $this->helper->getConfigCustomerGroup($storeId));
                    $orderStatus = explode(',', $this->helper->getConfigOrderStatus($storeId));

                    if (in_array($order->getStatus(), $orderStatus)) {
                        if (in_array(32000, $customerGroup)) {
                            $this->helper->sendReviewReminderMail($reminderModel);
                            $reminderModel->setData('sent_count', $reminderModel->getData('sent_count') + 1);
                        } else if (in_array($order->getCustomerGroupId(), $customerGroup)) {
                            $this->helper->sendReviewReminderMail($reminderModel);
                            $reminderModel->setData('sent_count', $reminderModel->getData('sent_count') + 1);
                        }
                    }
                }

                $this->helper->saveReminder($reminderModel);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

}
