<?php

namespace Meetanshi\ReviewReminder\Cron;

use Meetanshi\ReviewReminder\Helper\Data;
use Meetanshi\ReviewReminder\Model\ResourceModel\Reminder\Grid\CollectionFactory;
use Meetanshi\ReviewReminder\Model\Reminder as ReminderFactory;

/**
 * Class Reminder
 * @package Meetanshi\ReviewReminder\Cron
 */
class Reminder
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var CollectionFactory
     */
    private $reminderFactory;
    /**
     * @var ReminderFactory
     */
    private $reminderModel;

    /**
     * Reminder constructor.
     * @param Data $helper
     * @param CollectionFactory $reminderFactory
     * @param ReminderFactory $reminderModel
     */
    public function __construct(
        Data $helper,
        CollectionFactory $reminderFactory,
        ReminderFactory $reminderModel
    )
    {
        $this->helper = $helper;
        $this->reminderModel = $reminderModel;
        $this->reminderFactory = $reminderFactory;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        if ($this->helper->getConfig()):
            try {
                $collection = $this->reminderFactory->create();
                $collection->addFieldToFilter('sent_count', array('lt' => $this->helper->getConfigCounter()));
                $collection->addFieldToFilter('status', array('like' => 'pending%'));

                if ($collection->count()):
                    foreach ($collection as $reminder):
                        $customerGroup = $orderStatus = array();
                        $customerGroup = explode(',', $this->helper->getConfigCustomerGroup($reminder->getStoreId()));
                        $orderStatus = explode(',', $this->helper->getConfigOrderStatus($reminder->getStoreId()));
                        $order = $this->helper->getOrderFromIncrementId($reminder->getIncrementId());

                        if ($this->helper->getOrderFromIncrementId($reminder->getIncrementId()) && $this->helper->getConfig($reminder->getStoreId())):
                            if (!$this->helper->getProductByStore($reminder->getProductId(), $reminder->getStoreId())) {
                                continue;
                            }
                            $currentDate = date_create($this->helper->getCurrentTime());
                            $orderDate = date_create($order->getCreatedAt());
                            $dateDiff = date_diff($currentDate, $orderDate);
                            $dateDiff = $dateDiff->format('%a');
                            $flag = ($dateDiff >= $this->helper->getDays($reminder->getStoreId()) ? true : false);
                            $customerGroupFlage = false;
                            if (in_array("32000", $customerGroup)) {
                                $customerGroupFlage = true;
                            } elseif (in_array($order->getCustomerGroupId(), $customerGroup)) {
                                $customerGroupFlage = true;
                            }
                            if (in_array($order->getStatus(), $orderStatus)
                                AND $customerGroupFlage
                                AND $flag):
                                $this->helper->sendReviewReminderMail($reminder);
                                $reminderModel = $this->reminderModel->load($reminder->getReminderId());
                                $reminderModel->setData('sent_count', $reminderModel->getData('sent_count') + 1);
                                $reminderModel->setData('mail_status', 'sent');
                                $reminderModel = $this->helper->saveReminder($reminderModel);
                            endif;
                        endif;
                    endforeach;
                endif;
            } catch (\Exception $e) {
                $this->helper->log($e->getMessage());
            }
        endif;
        return $this;
    }
}
