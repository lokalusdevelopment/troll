<?php

namespace Meetanshi\ReviewReminder\Block\Mail;

use Magento\Framework\View\Element\Template as CoreTemplate;
use Magento\Catalog\Model\ProductRepository;
use Meetanshi\ReviewReminder\Helper\Data;
use Magento\Catalog\Helper\Image;

/**
 * Class Template
 * @package Meetanshi\ReviewReminder\Block\Mail
 */
class Template extends CoreTemplate
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Image
     */
    private $imageHelperFactory;

    /**
     * Template constructor.
     * @param CoreTemplate\Context $context
     * @param array $data
     * @param ProductRepository $productRepository
     * @param Data $helper
     * @param Image $imageHelperFactory
     */
    public function __construct
    (
        CoreTemplate\Context $context,
        array $data = [],
        ProductRepository $productRepository,
        Data $helper,
        Image $imageHelperFactory
    )
    {
        $this->productRepository = $productRepository;
        $this->helper = $helper;
        $this->imageHelperFactory = $imageHelperFactory;
        CoreTemplate::__construct($context, $data);
    }

    /**
     * @param $productId
     * @param $storeId
     * @return \Magento\Catalog\Api\Data\ProductInterface|null
     */
    public function getProductByStore($productId, $storeId)
    {
        try {
            $prod = $this->productRepository->getById($productId, false, $storeId);
            return $prod;
        } catch (\Exception $e) {
            return null;
        }

    }

    /**
     * @param $storeId
     * @return bool|string
     */
    public function getUtm($storeId)
    {
        return $this->helper->getUtmConfig($storeId);
    }

    /**
     * @param $product
     * @return string
     */
    public function getProductImage($product)
    {
        try {

            return $this->imageHelperFactory
                ->init($product, 'small_image', ['type' => 'small_image'])
                ->keepAspectRatio(true)->resize('65', '65')
                ->getUrl();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * @param $incrementId
     * @return mixed
     */
    public function getStoreId($incrementId)
    {
        $order = $this->helper->getOrderFromIncrementId($incrementId);
        return $order->getStoreId();
    }

    /**
     * @return Data
     */
    public function getReminderHelper()
    {
        return $this->helper;
    }
}
