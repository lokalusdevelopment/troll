<?php


namespace Meetanshi\ReviewReminder\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Store\Model\ScopeInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\Catalog\Model\ProductRepository;

/**
 * Class Data
 * @package Meetanshi\ReviewReminder\Helper
 */
class Data extends AbstractHelper
{
    const ENABLE = 'admin_reviewreminder/config/enable';

    const DAYS = 'admin_reviewreminder/config/general/days';
    const CUSTOMER = 'admin_reviewreminder/config/general/customer';
    const COUNT = 'admin_reviewreminder/config/general/count';
    const SENDER = 'admin_reviewreminder/config/general/sender';
    const EMAIL_TEMPLATE = 'admin_reviewreminder/config/general/email_template';
    const BCC = 'admin_reviewreminder/config/general/bcc';
    const LOG = 'admin_reviewreminder/config/general/log';
    const STATUS = 'admin_reviewreminder/config/general/status';

    const RULE = 'admin_reviewreminder/config/coupon/rule';
    const RULE_TEMPLATE = 'admin_reviewreminder/config/coupon/rule_template';

    const UTM_ENABLE = 'admin_reviewreminder/config/utm/enable';
    const UTM_SOURCE = 'admin_reviewreminder/config/utm/source';
    const UTM_MEDIUM = 'admin_reviewreminder/config/utm/medium';
    const UTM_NAME = 'admin_reviewreminder/config/utm/name';

    /**
     * @var TimezoneInterface
     */
    private $timezone;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var StateInterface
     */
    private $inlineTranslation;
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;
    /**
     * @var OrderInterface
     */
    private $orderRepository;
    /**
     * @var Repository
     */
    private $assetRepo;
    /**
     * @var ProductUrlPathGenerator
     */
    private $productUrlPathGenerator;
    /**
     * @var ProductRepository
     */
    private $productRepository;


    /**
     * Data constructor.
     * @param Context $context
     * @param TimezoneInterface $timezone
     * @param StoreManagerInterface $storeManager
     * @param StateInterface $inlineTranslation
     * @param OrderInterface $orderRepository
     * @param Repository $assetRepo
     * @param TransportBuilder $transportBuilder
     * @param ProductRepository $productRepository
     * @param ProductUrlPathGenerator $productUrlPathGenerator
     */
    public function __construct(
        Context $context,
        TimezoneInterface $timezone,
        StoreManagerInterface $storeManager,
        StateInterface $inlineTranslation,
        OrderInterface $orderRepository,
        Repository $assetRepo,
        TransportBuilder $transportBuilder,
        ProductRepository $productRepository,
        ProductUrlPathGenerator $productUrlPathGenerator
    )
    {
        $this->timezone = $timezone;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->orderRepository = $orderRepository;
        $this->assetRepo = $assetRepo;
        $this->productUrlPathGenerator = $productUrlPathGenerator;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return bool
     */
    public function getConfig($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;;
        try {
            if ($this->scopeConfig->getValue(self::ENABLE, $scope, $storeId)):
                return true;
            else:
                return false;
            endif;

        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getConfigOrderStatus($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(self::STATUS, $scope, $storeId);

    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getConfigCustomerGroup($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(self::CUSTOMER, $scope, $storeId);

    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getConfigCounter($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(self::COUNT, $scope, $storeId);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getDays($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(self::DAYS, $scope, $storeId);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getCartRule($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(self::RULE, $scope, $storeId);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getEmailTemplate($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;;
        return $this->scopeConfig->getValue(self::EMAIL_TEMPLATE, $scope, $storeId);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getStoreName($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(
            'general/store_information/name',
            $scope,
            $storeId
        );
    }

    /**
     * @return string
     */
    public function getCurrentTime()
    {
        try {
            return $this->timezone->date()->format('Y-m-d H:i:s');

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * @param null $storeId
     * @return bool|string
     */
    public function getUtmConfig($storeId = null)
    {
        $scope = ScopeInterface::SCOPE_STORE;

        if ($this->scopeConfig->getValue(self::UTM_ENABLE, $scope, $storeId)) {
            if ($this->scopeConfig->getValue(self::UTM_SOURCE, $scope, $storeId)) {
                $utmUrl = '&utm_source=' . $this->scopeConfig->getValue(self::UTM_SOURCE, $scope);
                if ($utmMedium = $this->scopeConfig->getValue(self::UTM_MEDIUM, $scope, $storeId))
                    $utmUrl .= "&utm_medium=" . $utmMedium;
                if ($utmName = $this->scopeConfig->getValue(self::UTM_NAME, $scope, $storeId))
                    $utmUrl .= "&utm_campaign=" . $utmName;
                return $utmUrl;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $reminder
     * @return $this|string
     */
    public function sendReviewReminderMail($reminder)
    {
        try {
            $order = $this->getOrderFromIncrementId($reminder->getIncrementId());
            $config['template'] = $this->getEmailTemplate($reminder->getStoreId());
            $config['image'] = "Meetanshi_ReviewReminder::images/reminder.jpg";
            $config['reminderId'] = $reminder->getReminderId();
            $config['incrementId'] = $reminder->getIncrementId();
            $config['date'] = date("M d, Y h:i:s A", strtotime($reminder->getcreatedAt()));;
            $config['mail'] = $reminder->getMail();
            $config['store'] = $this->getStoreName($reminder->getStoreId());
            $config['storeId'] = $reminder->getStoreId();
            $config['reminder'] = $reminder->getData();
            $config['customer'] = $order->getBillingAddress()->getFirstName();
            $this->inlineTranslation->suspend();
            $this->generateTemplate($config);
            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            return $this;
        } catch (\Exception $e) {
            return $e->getMessage();
        }


    }

    /**
     * @param $incrementId
     * @return bool
     */
    public function getOrderFromIncrementId($incrementId)
    {
        try {
            $order = $this->orderRepository->loadByIncrementId($incrementId);
            return $order;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $config
     * @return $this|string
     */
    public function generateTemplate($config)
    {
        try {
            $this->transportBuilder->setTemplateIdentifier($config['template'])
                ->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => $config['storeId'],
                    ]
                )
                ->setTemplateVars($config)
                ->setFrom($this->scopeConfig->getValue(self::SENDER, ScopeInterface::SCOPE_STORE))
                ->addTo($config['mail']);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $this;
    }

    /**
     * @param $reminder
     * @param $config
     * @return $this|string
     */
    public function sendCouponCodeMail($reminder, $config)
    {
        try {
            $order = $this->getOrderFromIncrementId($reminder->getIncrementId());

            $config['template'] = $this->getRuleEmailTemplate($reminder->getStoreId());
            $config['mail'] = $reminder->getMail();
            $config['storeId'] = $reminder->getStoreId();
            $config['customer'] = $order->getBillingAddress()->getFirstName();
            $config['image'] = "Meetanshi_ReviewReminder::images/thankyou.jpg";

            $this->inlineTranslation->suspend();

            $this->generateTemplate($config);
            $transport = $this->transportBuilder->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();

            return $this;
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * @param $storeId
     * @return mixed
     */
    public function getRuleEmailTemplate($storeId)
    {
        $scope = ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->getValue(self::RULE_TEMPLATE, $scope, $storeId);
    }

    /**
     * @param $reminderModel
     * @return string
     */
    public function saveReminder($reminderModel)
    {
        try {
            return $reminderModel->save();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * @param $storeId
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreUrl($storeId)
    {
        return $this->storeManager->getStore($storeId)->getBaseUrl();
    }

    /**
     * @param $product
     * @param $storeId
     * @return string
     */
    public function getProductUrl($product, $storeId)
    {
        return $this->productUrlPathGenerator->getUrlPathWithSuffix($product, $storeId);
    }

    /**
     * @param $productIds
     * @param $storeId
     * @return bool
     */
    public function getProductByStore($productIds, $storeId)
    {
        try {
            $productIds = explode(',', $productIds);
            foreach ($productIds as $productId):
                $prod = $this->productRepository->getById($productId, false, $storeId);
                if ($prod == null) {
                    return false;
                }
            endforeach;
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * @param $msg
     */
    public function log($msg)
    {
        $this->_logger->info($msg);
    }
}
