<?php

namespace Meetanshi\ReviewReminder\Controller\Adminhtml\Reminder;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Meetanshi\ReviewReminder\Helper\Data;
use Meetanshi\ReviewReminder\Model\ReminderFactory;
use Meetanshi\ReviewReminder\Model\ResourceModel\Reminder\Grid\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 * @package Meetanshi\ReviewReminder\Controller\Adminhtml\Reminder
 */
class MassDelete extends Action
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ReminderFactory
     */
    private $reminderModel;
    /**
     * @var Filter
     */
    protected $filter;
    /**
     * @var CollectionFactory
     */
    private $reminderFactory;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Data $helper
     * @param ReminderFactory $reminderModel
     * @param Filter $filter
     * @param CollectionFactory $reminderFactory
     */
    public function __construct(
        Context $context,
        Data $helper,
        ReminderFactory $reminderModel,
        Filter $filter,
        CollectionFactory $reminderFactory
    )
    {
        $this->helper = $helper;
        $this->reminderModel = $reminderModel;
        $this->reminderFactory = $reminderFactory;
        $this->filter = $filter;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try {
            $collection = $this->filter->getCollection($this->reminderFactory->create());
            $counter = 0;
            foreach ($collection as $item) {
                $reminderModel = $this->reminderModel->create()->load($item['reminder_id']);
                $reminderModel->delete();
                $counter++;
            }
            $this->messageManager->addSuccessMessage(__($counter . ' Successfully Deleted'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_ReviewReminder::reminder_index');
    }
}
