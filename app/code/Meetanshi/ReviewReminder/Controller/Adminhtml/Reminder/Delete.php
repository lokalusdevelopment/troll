<?php

namespace Meetanshi\ReviewReminder\Controller\Adminhtml\Reminder;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Meetanshi\ReviewReminder\Helper\Data;
use Meetanshi\ReviewReminder\Model\ReminderFactory;

/**
 * Class Delete
 * @package Meetanshi\ReviewReminder\Controller\Adminhtml\Reminder
 */
class Delete extends Action
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var ReminderFactory
     */
    private $reminderModel;

    /**
     * Delete constructor.
     * @param Context $context
     * @param Data $helper
     * @param ReminderFactory $reminderModel
     */
    public function __construct(
        Context $context,
        Data $helper,
        ReminderFactory $reminderModel
    )
    {
        $this->helper = $helper;
        $this->reminderModel = $reminderModel;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try {
            $rowId = (int)$this->getRequest()->getParam('id');
            $reminderModel = $this->reminderModel->create()->load($rowId);
            $reminderModel->delete();
            $this->messageManager->addSuccessMessage(__($reminderModel->getIncrementId() . ' Successfully Deleted'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;

    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Meetanshi_ReviewReminder::reminder_index');
    }
}
