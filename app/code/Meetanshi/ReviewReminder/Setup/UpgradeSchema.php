<?php

namespace Meetanshi\ReviewReminder\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 * @package Meetanshi\ReviewReminder\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context ) {
        $installer = $setup;

        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') <= 0) {
            $installer->getConnection()->addColumn(
                $installer->getTable('meetanshi_review_reminder'),
                'store_id',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'length' => '50',
                    'default' => '0',
                    'comment' => 'Store Id'
                ]
            );
        }
        $installer->endSetup();
    }
}
