<?php


namespace Meetanshi\ReviewReminder\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Meetanshi\ReviewReminder\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return string|void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        try {
            $installer = $setup;
            $installer->startSetup();
            if (!$installer->tableExists('meetanshi_review_reminder')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('meetanshi_review_reminder')
                )
                    ->addColumn(
                        'reminder_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'nullable' => false,
                            'primary' => true,
                            'unsigned' => true,
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'created_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                        'Created At'
                    )
                    ->addColumn(
                        'mail_status',
                        Table::TYPE_TEXT,
                        255,
                        ['default' => 'pending'],
                        'Mail Status'
                    )
                    ->addColumn(
                        'product_id',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'Product ID'
                    )
                    ->addColumn(
                        'sent_count',
                        Table::TYPE_TEXT,
                        255,
                        ['default' => '0'],
                        'Post Post Content'
                    )
                    ->addColumn(
                        'increment_id',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'Order Number'
                    )
                    ->addColumn(
                        'mail',
                        Table::TYPE_TEXT,
                        255,
                        [],
                        'Order Mail'
                    )
                    ->addColumn(
                        'status',
                        Table::TYPE_TEXT,
                        255,
                        ['default' => 'pending'],
                        'Status'
                    )
                    ->setComment('Review Reminder');
                $installer->getConnection()->createTable($table);
            }
            $installer->endSetup();
        } catch (\Zend_Db_Exception $e) {
            return $e->getMessage();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }
}
