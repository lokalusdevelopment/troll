<?php


namespace Meetanshi\ReviewReminder\Ui\Component\Listing\Renderer;

use Magento\Framework\Escaper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\System\Store as SystemStore;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Meetanshi\ReviewReminder\Helper\Data;

/**
 * Class IncrementId
 * @package Meetanshi\ReviewReminder\Ui\Component\Listing\Renderer
 */
class IncrementId extends Column
{
    /**
     * @var Escaper
     */
    private $escaper;
    /**
     * @var SystemStore
     */
    private $systemStore;
    /**
     * @var OrderInterface
     */
    private $orderRepository;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Data
     */
    private $helper;

    /**
     * IncrementId constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param SystemStore $systemStore
     * @param Escaper $escaper
     * @param Data $helper
     * @param StoreManagerInterface $storeManager
     * @param OrderInterface $orderRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SystemStore $systemStore,
        Escaper $escaper,
        Data $helper,
        StoreManagerInterface $storeManager,
        OrderInterface $orderRepository,
        array $components = [],
        array $data = []
    )
    {

        $this->systemStore = $systemStore;
        $this->escaper = $escaper;
        $this->orderRepository = $orderRepository;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array|string
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (array_key_exists('increment_id', $item) && !empty($item['increment_id'])) {
                    try {
                        if ($order = $this->helper->getOrderFromIncrementId($item['increment_id'])) {
                            $url = $this->storeManager->getStore()->getUrl('sales/order/view', ['order_id' => $order->getEntityId()]);
                            $item[$this->getData('name')] = html_entity_decode("<a target='_blank' href=\"$url\">" . $order->getIncrementId() . "</a>");
                        }
                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
            }
        }

        return $dataSource;
    }
}
