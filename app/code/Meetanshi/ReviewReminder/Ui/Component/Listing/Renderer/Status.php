<?php


namespace Meetanshi\ReviewReminder\Ui\Component\Listing\Renderer;

use Magento\Framework\Escaper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\System\Store as SystemStore;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Status
 * @package Meetanshi\ReviewReminder\Ui\Component\Listing\Renderer
 */
class Status extends Column
{
    /**
     * @var Escaper
     */
    private $escaper;
    /**
     * @var SystemStore
     */
    private $systemStore;

    /**
     * Status constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param SystemStore $systemStore
     * @param Escaper $escaper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SystemStore $systemStore,
        Escaper $escaper,
        array $components = [],
        array $data = []
    )
    {

        $this->systemStore = $systemStore;
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array|string
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                try {
                    if ($item[$this->getData('name')] == 'pending'):
                        $item[$this->getData('name')] = html_entity_decode("<span style='font-size: 16px;color: red;font-weight: bold;'>" . ucfirst($item[$this->getData('name')]) . "</span>");
                    else:
                        $item[$this->getData('name')] = html_entity_decode("<span style='font-size: 16px;color: green;font-weight: bold;' >" . ucfirst($item[$this->getData('name')]) . "</span>");
                    endif;

                } catch (\Exception $e) {
                    return $e->getMessage();
                }

            }
        }
        return $dataSource;
    }
}
