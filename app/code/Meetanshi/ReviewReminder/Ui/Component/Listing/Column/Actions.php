<?php


namespace Meetanshi\ReviewReminder\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;


/**
 * Class Actions
 * @package Meetanshi\ReviewReminder\Ui\Component\Listing\Column
 */
class Actions extends Column
{

    /**
     *
     */
    const URL_PATH_PREVIEW = 'review_reminder/reminder/preview';
    /**
     *
     */
    const URL_PATH_SEND = 'review_reminder/reminder/send';
    /**
     *
     */
    const URL_PATH_DELETE = 'review_reminder/reminder/delete';

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Actions constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    )
    {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }


    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if ( isset($dataSource['data']['items']) ) {
            foreach ($dataSource['data']['items'] as & $item) {
                if ( isset($item['reminder_id']) ) {
                    $item[$this->getData('name')] = [
                        'send' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_SEND,
                                [
                                    'id' => $item['reminder_id']
                                ]
                            ),
                            'label' => __('Send')
                        ],
                        'delete' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'id' => $item['reminder_id']
                                ]
                            ),
                            'label' => __('Delete'),
                            'confirm' => [
                                'title' => __('Delete'),
                                'message' => __('Are you sure you wan\'t to delete a reminder?')
                            ]
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
