<?php


namespace Meetanshi\ReviewReminder\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class MailStatus
 * @package Meetanshi\ReviewReminder\Ui\Component\Listing\Column
 */
class MailStatus implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'pending',
                'label' => __('Pending'),
            ],
            [
                'value' => 'sent',
                'label' => __('Sent'),
            ]
        ];
    }
}
