<?php


namespace Meetanshi\ReviewReminder\Ui\Component\Listing\Column;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 * @package Meetanshi\ReviewReminder\Ui\Component\Listing\Column
 */
class Status implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'pending',
                'label' => __('Pending'),
            ],
            [
                'value' => 'submitted',
                'label' => __('Submitted'),
            ]
        ];
    }
}
