<?php

namespace Meetanshi\ReviewReminder\Api\Data;

/**
 * Interface ReminderInterface
 * @package Meetanshi\ReviewReminder\Api\Data
 */
interface ReminderInterface
{
    const REMINDER_ID = 'reminder_id';
    const MAIL_STATUS = 'mail_status';
    const SENT_COUNT = 'sent_count';
    const INCREMENT_ID = 'increment_id';
    const MAIL = 'mail';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';

    /**
     * @return mixed
     */
    public function getReminderId();

    /**
     * @param $reminderId
     * @return mixed
     */
    public function setReminderId($reminderId);

    /**
     * @return mixed
     */
    public function getMailStatus();

    /**
     * @param $mailStatus
     * @return mixed
     */
    public function setMailStatus($mailStatus);

    /**
     * @return mixed
     */
    public function getSentCount();

    /**
     * @param $sentCount
     * @return mixed
     */
    public function setSentCount($sentCount);

    /**
     * @return mixed
     */
    public function getIncrementId();

    /**
     * @param $incrementId
     * @return mixed
     */
    public function setIncrementId($incrementId);

    /**
     * @return mixed
     */
    public function getMail();

    /**
     * @param $mail
     * @return mixed
     */
    public function setMail($mail);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param $createdAt
     * @return mixed
     */
    public function setCreatedAt($createdAt);
}
