<?php

namespace KZCode\XlsSalesExport\Controller\Adminhtml\Export;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;
use XLSXWriter;

class Xls extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $filter);
    }

    protected function massAction(AbstractCollection $invoices)
    {
        foreach ($invoices as $invoice) {
            $date = date("Y.m.d", strtotime($invoice->getCreatedAt()));
            $invoiceId = $invoice->getIncrementId();
            $billingAddress = $invoice->getBillingAddress();
            $clientFullName = $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
            $companyNumber = '';

            if (!is_null($billingAddress->getCompany())) {
                $company = explode(',', $billingAddress->getCompany());
                $companyNumber = (isset($company[1])) ? trim($company[1]) : '';
            }

            $cashOnDeliveryPayment = 0;
            $cashOnDeliveryTax = 0;

            if (!is_null($invoice->getCashOnDeliveryFee()) && $invoice->getCashOnDeliveryFee() != 0) {
                $cashOnDeliveryPayment = 1.65;
                $cashOnDeliveryTax = 0.35;
            }

            $shippingTotal = round($invoice->getShippingAmount() + $cashOnDeliveryPayment, 2);
            $taxTotal = round($invoice->getTaxAmount() + $cashOnDeliveryTax, 2);
            $grandTotalWithoutTax = round($invoice->getBaseGrandTotal() - $taxTotal - $shippingTotal, 2);

            $invoiceToExport[] = [
                'I',
                $date,
                'TN',
                $invoiceId,
                '',
                $clientFullName,
                $companyNumber,
                '2412',
                '5000',
                $grandTotalWithoutTax,
            ];
            $invoiceToExport[] = [
                'I',
                $date,
                'TN',
                $invoiceId,
                '',
                $clientFullName,
                $companyNumber,
                '2412',
                '5221',
                $shippingTotal,
            ];

            $invoiceToExport[] = [
                'I',
                $date,
                'TN',
                $invoiceId,
                '',
                $clientFullName,
                $companyNumber,
                '2412',
                '44840',
                $taxTotal,
            ];
        }

        if ($invoiceToExport) {
            $header = array(
                'Tipas' => 'string',
                'Data' =>'string',
                'Serija' => 'string',
                'Numeris' => 'string',
                'Valstybės kodas' => 'string',
                'Pavadinimas/ Vardas, Pavardė' => 'string',
                'Įmonės kodas' => 'string',
                'Sąskaita debete' => 'string',
                'Sąskaita kredite' => 'string',
                'Suma' => 'string',
            );

            $writer = new XLSXWriter();
            $writer->writeSheet($invoiceToExport, 'Sheet1', $header);
            $file = 'Invoice-' . $invoiceId . '.xlsx';
            $writer->writeToFile($file);

            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
                unlink($file);
                exit;
            }
        }

        return true;
    }
}
