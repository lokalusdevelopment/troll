<?php

namespace KZCode\Custom\Plugin\Order\Email\Container;

class Template
{
    public function beforeSetTemplateVars(\Magento\Sales\Model\Order\Email\Container\Template $subject, array $vars)
    {
        /** @var Order $order */
        $order = $vars['order'];
        $shippingMethod = $order->getShippingMethod();
        $payment = $order->getPayment();
        $paymentMethod = $payment->getMethod();

        $vars['is_'.$shippingMethod] = $shippingMethod;
        $vars['is_'.$paymentMethod] = $paymentMethod;

        return [$vars];
    }
}
