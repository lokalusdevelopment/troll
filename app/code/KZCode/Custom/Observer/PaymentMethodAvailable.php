<?php

namespace KZCode\Custom\Observer;

use Magento\Framework\Event\ObserverInterface;
use \Magento\Checkout\Model\Cart;

class PaymentMethodAvailable implements ObserverInterface
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * PaymentMethodAvailable constructor.
     * @param Cart $cart
     */
    public function __construct(
        Cart $cart ){
        $this->cart = $cart;
    }
    /**
     * payment_method_is_active event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $paymentMethod = $observer->getEvent()->getMethodInstance()->getCode();
        $shippingMethod = $shippingMethod = $this->cart->getQuote()->getShippingAddress()->getShippingMethod();

        // you can replace "checkmo" with your required payment method code
        if ($paymentMethod == "checkmo" && ($shippingMethod == "dpd_pickup" || $shippingMethod == "dpd_classic")) {
            $checkResult = $observer->getEvent()->getResult();
            $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
        }

        if ($paymentMethod == "cashondelivery" && $shippingMethod == "freeshipping_freeshipping") {
            $checkResult = $observer->getEvent()->getResult();
            $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
        }
    }
}
