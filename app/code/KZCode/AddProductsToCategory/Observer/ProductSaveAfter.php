<?php

namespace KZCode\AddProductsToCategory\Observer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductSaveAfter implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $product = $observer->getProduct();
        $this->setProductNewCategory($product);
        $this->setProductDiscountCategory($product);

        return true;
    }

    private function setProductNewCategory($_product)
    {
        $_categoryId = 344;
        $_sku = $_product->getSku();
        $_objectManager = ObjectManager::getInstance();
        $_categoryIds = $_product->getCategoryIds();
        $_fromDate = date('Y-m-d', strtotime($_product->getNewsFromDate()));
        $_toDate = date('Y-m-d', strtotime($_product->getNewsToDate()));
        $_today = date("Y-m-d");

        if (!is_null($_product->getNewsFromDate()) && is_null($_product->getNewsToDate())) {
            $_toDate = date("Y-m-d", time() + 186400);
        }

        if (is_null($_product->getNewsFromDate()) && !is_null($_product->getNewsToDate())) {
            $_fromDate = date("Y-m-d", time() - 186400);
        }

        if (($_today >= $_fromDate) && ($_today <= $_toDate)) {
            if (!in_array($_categoryId, $_categoryIds)) {
                array_push($_categoryIds, $_categoryId);
                $_product->setCategoryIds($_categoryIds);
                $_product->save();
            }
        } else {
            if (in_array($_categoryId, $_categoryIds)) {
                $categoryLinkRepository = $_objectManager->get('\Magento\Catalog\Model\CategoryLinkRepository');
                $categoryLinkRepository->deleteByIds($_categoryId, $_sku);
            }
        }

        return true;
    }

    private function setProductDiscountCategory($_product)
    {
        $_categoryId = 289;
        $_sku = $_product->getSku();
        $_objectManager = ObjectManager::getInstance();
        $_categoryIds = $_product->getCategoryIds();
        $_fromDate = date('Y-m-d', strtotime($_product->getSpecialFromDate()));
        $_toDate = date('Y-m-d', strtotime($_product->getSpecialToDate()));
        $_today = date("Y-m-d");

        if (!is_null($_product->getSpecialFromDate()) && is_null($_product->getSpecialToDate())) {
            $_toDate = date("Y-m-d", time() + 186400);
        }

        if (is_null($_product->getSpecialFromDate()) && !is_null($_product->getSpecialToDate())) {
            $_fromDate = date("Y-m-d", time() - 186400);
        }

        if ((($_today >= $_fromDate) && ($_today <= $_toDate)) && !is_null($_product->getSpecialPrice())) {
            if (!in_array($_categoryId, $_categoryIds)) {
                array_push($_categoryIds, $_categoryId);
                $_product->setCategoryIds($_categoryIds);
                $_product->save();
            }
        } else {
            if (in_array($_categoryId, $_categoryIds)) {
                $categoryLinkRepository = $_objectManager->get('\Magento\Catalog\Model\CategoryLinkRepository');
                $categoryLinkRepository->deleteByIds($_categoryId, $_sku);
            }
        }

        return true;
    }
}
