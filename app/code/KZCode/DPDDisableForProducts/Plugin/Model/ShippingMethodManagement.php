<?php
namespace KZCode\DPDDisableForProducts\Plugin\Model;

use Magento\Checkout\Model\Cart;

class ShippingMethodManagement {

    public function __construct(
        Cart $cart,
        \Magento\Catalog\Model\ProductFactory $product
    )
    {
        $this->cart = $cart;
        $this->product = $product;
    }

    public function afterEstimateByExtendedAddress($shippingMethodManagement, $output)
    {
        return $this->filterOutput($output);
    }

    private function filterOutput($output)
    {
        $disableDpdPickup = false;
        $allItems = $this->cart->getQuote()->getAllItems();

        // iterate all cart products to check if no_free_shipping is true
        foreach ($allItems as $item) {
            $_product = $this->product->create()->load($item->getProduct()->getId());
            // if product has no_free_shipping true
            if ($_product->getDisableDbdPickup()) {
                $disableDpdPickup = true;
                break;
            }
        }

        if ($disableDpdPickup) {
            foreach ($output as $key => $shippingMethod) {
                if ($shippingMethod->getMethodCode() == 'pickup' && $shippingMethod->getCarrierCode() == 'dpd') {
                    unset($output[$key]);
                }
            }

            return $output;
        }

        return $output;
    }
}
