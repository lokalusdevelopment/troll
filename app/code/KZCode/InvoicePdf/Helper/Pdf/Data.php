<?php

namespace KZCode\InvoicePdf\Helper\Pdf;

use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Directory\Model\CountryFactory;

class Data extends AbstractHelper
{

    public $countryFactory;

    public function __construct(
        CountryFactory $countryFactory
    ) {
        $this->countryFactory = $countryFactory;
    }

    public function getPdfSettings()
    {
        return [
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_top' => 5,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10,
            'default_font' => 'dejavusans'
        ];
    }

    public function getFooter()
    {
        return '
        <div width="100%" style="text-align:center; width: 100%; font-size: 10px;">
            Trolių namas | Prekės vaikams - Vytauto g. 100 - LT-00133 Palanga - Lietuva<br />
            Norėdami gauti daugiau pagalbos, susisiekite:<br />
            Tel.: +37061316239<br />
            Įmonės kodas: 166929889 PVM mokėtojo kodas: LT669298811
        </div>';
    }

    public function getCorrectDate($date)
    {
        $date = strftime('%Y m. %B mėn. %d d.', strtotime($date));
        $search = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $replace = array('Sausio', 'Vasario', 'Kovo', 'Balandžio', 'Gegužės', 'Birželio', 'Liepos', 'Rugpjūčio', 'Rūgsėjo', 'Spalio', 'Lapkričio', 'Gruodžio');
        $date = str_replace($search, $replace, $date);

        return $date;
    }

    public function getBillingAddress($billingAddress)
    {
        $company = $billingAddress->getCompany();
        
        if (is_null($company)) {
            $text = 
                $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName() . '<br />
                ' . $billingAddress->getStreetLine(1) . '<br />
                ' . $billingAddress->getPostcode() . ' ' . $billingAddress->getCity() . '<br />
                ' . $this->getCountryName($billingAddress->getCountryId()) . '<br />
                ' . $billingAddress->getTelephone() . '<br />';
        } else {

            $company = explode(',', $company);
            $companyName = $company[0];
            $companyNumber = (isset($company[1])) ? $company[1] : '';

            $text = 
                $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName() . '<br /> ' .
                $companyName . '<br />
                Įmonės kodas ' . $companyNumber . '<br />
                PVM kodas ' . $billingAddress->getVatId() . '<br />
                Adresas: ' . $billingAddress->getStreetLine(1) . ',
                ' . $billingAddress->getPostcode() . ' ' . $billingAddress->getCity() . ',
                ' . $this->getCountryName($billingAddress->getCountryId()) . '<br />
                ' . $billingAddress->getTelephone() . '<br />';
        }

        return $text;
    }

    public function getCountryName($countryId): string
    {
        $countryName = '';
        $country = $this->countryFactory->create()->loadByCode($countryId);
        if ($country) {
            $countryName = $country->getName();
        }
        return $countryName;
    }
}
