<?php

namespace KZCode\InvoicePdf\Controller\Adminhtml\Invoice;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Directory\Model\CountryFactory;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;

class Create extends Action
{
    protected $orderRepository;
    protected $invoiceService;
    protected $transaction;
    protected $invoiceSender;

    public $countryFactory;

    public function __construct(
        Context $context,
        OrderRepositoryInterface $orderRepository,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        CountryFactory $countryFactory,
        Data $pricingHelper,
        \KZCode\InvoicePdf\Helper\Pdf\Data $helper,
        Transaction $transaction
    )
    {
        $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->invoiceSender = $invoiceSender;
        $this->countryFactory = $countryFactory;
        $this->pricingHelper = $pricingHelper;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        return true;
    }

    public function createInvoice($order, $output = 'D')
    {
        $mpdf = new Mpdf($this->pdfSettings());
        $mpdf->SetHTMLFooter($this->getPdfFooter());
        $invoices = $order->getInvoiceCollection();
        $page = 1;

        foreach ($invoices as $invoice) {
            setlocale(LC_ALL, 'lt_LT.UTF-8');
            $date = $this->helper->getCorrectDate($invoice->getCreatedAt());
            $billingAddress = $invoice->getBillingAddress();
            $mpdf->SetTitle('TN' . $invoice->getIncrementId());
            $cashOnDeliveryPayment = 0;
            $cashOnDeliveryTax = 0;

            $mpdf->WriteHTML('
            <div style="text-align: center; font-size: 14px; margin-bottom: 50px;">
                PVM SĄSKAITA-FAKTŪRA<br />
                Serija Nr.TN' . $invoice->getIncrementId() . '<br />
                ' . $date . '
            </div>
            <table style="width: 100%; font-size: 14px; margin: 0; padding: 0">
                <tr>
                    <td style="width: 50%; display: inline-block; vertical-align:top">
                        PARDAVĖJAS<br /><br />
                        UAB "TROLIŲ NAMAS"<br />
                        Įmonės kodas 166929889<br />
                        PVM kodas LT669298811<br />
                        Adresas: Žemaitijos 20, 89151 Mažeikiai<br />
                        LT667300010129253405 Swedbankas AB
                    </td>
                    <td style="width: 50%; display: inline-block">
                        PIRKĖJAS<br /><br />' . $this->helper->getBillingAddress($billingAddress) . '<br />
                    </td>
                </tr>
            </table>
            ');

            $mpdf->WriteHTML('
            <table style="width: 100%; border: 2px solid #000000; margin-top: 20px; font-size: 12px; border-spacing: 0;">
                <tr style="background-color: #DCDCDC; border: 1px solid red;">
                    <td style="width: 50px; text-align: center;">Nr.</td>
                    <td style="text-align: center">Kodas</td>
                    <td style="padding-left: 10px">Pavadinimas</td>
                    <td style="width: 65px; text-align: center">Mato vnt</td>
                    <td style="width: 50px; text-align: center">Kiekis</td>
                    <td style="width: 70px; text-align: center">Kaina su PVM.</td>
                    <td style="width: 70px; text-align: center">Kaina be PVM.</td>
                    <td style="width: 70px; text-align: center">Suma be PVM Eur.</td>
                </tr>
        ');

            $items = $invoice->getOrder()->getAllItems();
            $i = 1;

            foreach ($items as $item) {
                $mpdf->WriteHTML('
                    <tr>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $i++ . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $item->getSku() . '</td>
                        <td style="padding: 10px; border-top: 1px solid #000000;">' . $item->getName() . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . (int)$item->getQtyInvoiced() . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getPriceInclTax(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getPrice(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getRowTotal(), true, false) . '</td>
                    </tr>
                ');
            }

            if ($invoice->getCashOnDeliveryFee() > 0) {
                $mpdf->WriteHTML('
                    <tr>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $i++ . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;"></td>
                        <td style="padding: 10px; border-top: 1px solid #000000;">Atsiskaitymas pristatymo metu</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">1</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee() / 1.21, true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee() / 1.21, true, false) . '</td>
                    </tr>
                ');

                $cashOnDeliveryPayment = 1.65;
                $cashOnDeliveryTax = 0.35;
            }

            $mpdf->WriteHTML('
                <tr>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $i . '</td>
                    <td style="text-align: center; border-top: 1px solid #000000;"></td>
                    <td style="padding: 10px; border-top: 1px solid #000000;">Pristatymas</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">1</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingInclTax(), true, false) . '</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingAmount(), true, false) . '</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingAmount(), true, false) . '</td>
                </tr>
            ');
            $mpdf->WriteHTML('</table>');
            $mpdf->WriteHTML('
                <table style="width: 100%; margin-top: 20px; font-size: 12px;">
                    <tr>
                        <td style="width: 90%; text-align: right;">Suma viso be PVM:</td>
                        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getBaseSubtotal() + $cashOnDeliveryPayment + $invoice->getShippingAmount(), true, false) . '</td>
                    </tr>
                    <tr>
                        <td style="width: 90%; text-align: right;">PVM 21%:</td>
                        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getBaseTaxAmount() + $cashOnDeliveryTax, true, false) . '</td>
                    </tr>
                    <tr>
                        <td style="width: 90%; text-align: right;">Suma su PVM:</td>
                        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getGrandTotal(), true, false) . '</td>
                    </tr>
                </table>
            ');

            if (count($invoices) > 1 && count($invoices) != $page) {
                $mpdf->AddPage();
            }
            $page++;
        }

        return $mpdf->Output('TN' . $invoice->getIncrementId() . '.pdf', $output);
    }

    private function pdfSettings()
    {
        return [
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_top' => 5,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10,
            'default_font' => 'dejavusans',
        ];
    }

    private function getPdfFooter()
    {
        return '<div width="100%" style="text-align:center; width: 100%; font-size: 10px;">
                    Trolių namas | Prekės vaikams - Vytauto g. 100 - LT-00133 Palanga - Lietuva<br />
                    Norėdami gauti daugiau pagalbos, susisiekite:<br />
                    Tel.: +37061316239<br />
                    Įmonės kodas: 166929889 PVM mokėtojo kodas: LT669298811
                </div>';
    }

    public function getCountryName($countryId): string
    {
        $countryName = '';
        $country = $this->countryFactory->create()->loadByCode($countryId);
        if ($country) {
            $countryName = $country->getName();
        }
        return $countryName;
    }
}
