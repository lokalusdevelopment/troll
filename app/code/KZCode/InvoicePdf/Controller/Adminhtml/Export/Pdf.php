<?php

namespace KZCode\InvoicePdf\Controller\Adminhtml\Export;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory;
use Mpdf\Mpdf;
use Magento\Directory\Model\CountryFactory;
use KZCode\InvoicePdf\Helper\Pdf\Data;

//@Todo sutvarkyti imones rekvizitus
//@Todo pasa;ingi pdf invice migtukas, arba paziureti, kaip ji galetu replaceinti
//@Todo supaprastinti koda ir iskelti kuo daugiau funkcionalumo i helperi

//@Todo nustatyti failo pavadinima, jai saskaita viena tada pdf pavadinimas invoice numeris, jai daugiau tada tiesiog invoice+data
//@Todo jai yra daugiau nei viena saskaita, tada jas sudeti ir suzipuoti i atskiras
//@Todo nustatymas kaip siusti kelias saskaitas ar sudezi i zip ar i viena faila
//@TODO menesius isversti

class Pdf extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    public $countryFactory;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        CountryFactory $countryFactory,
        Data $helper
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->countryFactory = $countryFactory;
        $this->pricingHelper = $pricingHelper;
        $this->helper = $helper;
        parent::__construct($context, $filter);
    }

    protected function massAction(AbstractCollection $invoices)
    {
        $mpdf = new Mpdf($this->helper->getPdfSettings());
        $mpdf->SetHTMLFooter($this->helper->getFooter());
        $p = 1;

        foreach ($invoices as $invoice) {
            setlocale(LC_ALL, 'lt_LT.UTF-8');
            $date = $this->helper->getCorrectDate($invoice->getCreatedAt());
            $billingAddress = $invoice->getBillingAddress();
            $mpdf->SetTitle('TN' . $invoice->getIncrementId());
            $cashOnDeliveryPayment = 0;
            $cashOnDeliveryTax = 0;

            $mpdf->WriteHTML('
            <div style="text-align: center; font-size: 18px; margin-bottom: 50px;">
                PVM SĄSKAITA-FAKTŪRA<br />
                Serija Nr.TN' . $invoice->getIncrementId() . '<br />
                ' . $date . '
            </div>
            <table style="width: 100%; font-size: 18px; margin: 0; padding: 0">
                <tr>
                    <td style="width: 50%; display: inline-block; vertical-align:top">
                        PARDAVĖJAS<br /><br />
                        UAB "TROLIŲ NAMAS"<br />
                        Įmonės kodas 166929889<br />
                        PVM kodas LT669298811<br />
                        Adresas: Žemaitijos 20, 89151 Mažeikiai<br />
                        LT667300010129253405 Swedbankas AB
                    </td>
                    <td style="width: 50%; display: inline-block; vertical-align:top">
                        PIRKĖJAS<br /><br />' . $this->helper->getBillingAddress($billingAddress) . '<br />
                    </td>
                </tr>
            </table>
            ');

            $mpdf->WriteHTML('
            <table style="width: 100%; border: 2px solid #000000; margin-top: 20px; font-size: 12px; border-spacing: 0;">
                <tr style="background-color: #DCDCDC; border: 1px solid red;">
                    <td style="width: 50px; text-align: center;">Nr.</td>
                    <td style="text-align: center">Kodas</td>
                    <td style="padding-left: 10px">Pavadinimas</td>
                    <td style="width: 65px; text-align: center">Mato vnt</td>
                    <td style="width: 50px; text-align: center">Kiekis</td>
                    <td style="width: 70px; text-align: center">Kaina su PVM.</td>
                    <td style="width: 70px; text-align: center">Kaina be PVM.</td>
                    <td style="width: 70px; text-align: center">Suma be PVM Eur.</td>
                </tr>
        ');


                $items = $invoice->getOrder()->getAllItems();
                $i = 1;

                foreach ($items as $item) {
                    $mpdf->WriteHTML('
                        <tr>
                            <td style="text-align: center; border-top: 1px solid #000000;">' . $i++ . '</td>
                            <td style="text-align: center; border-top: 1px solid #000000;">' . $item->getSku() . '</td>
                            <td style="padding: 10px; border-top: 1px solid #000000;">' . $item->getName() . '</td>
                            <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                            <td style="text-align: center; border-top: 1px solid #000000;">' . (int)$item->getQtyInvoiced() . '</td>
                            <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getPriceInclTax(), true, false) . '</td>
                            <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getPrice(), true, false) . '</td>
                            <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getRowTotal(), true, false) . '</td>
                        </tr>
                    ');
                }

            if ($invoice->getCashOnDeliveryFee() > 0) {
                $mpdf->WriteHTML('
                    <tr>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $i++ . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;"></td>
                        <td style="padding: 10px; border-top: 1px solid #000000;">Atsiskaitymas pristatymo metu</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">1</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee() / 1.21, true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee() / 1.21, true, false) . '</td>
                    </tr>
                ');

                $cashOnDeliveryPayment = 1.65;
                $cashOnDeliveryTax = 0.35;
            }

            $mpdf->WriteHTML('
                <tr>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $i . '</td>
                    <td style="text-align: center; border-top: 1px solid #000000;"></td>
                    <td style="padding: 10px; border-top: 1px solid #000000;">Pristatymas</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">1</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingInclTax(), true, false) . '</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingAmount(), true, false) . '</td>
                    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingAmount(), true, false) . '</td>
                </tr>
            ');
            $mpdf->WriteHTML('</table>');
            $mpdf->WriteHTML('

<table style="width: 100%; margin-top: 20px; font-size: 12px;">
    <tr>
        <td style="width: 90%; text-align: right;">Suma viso be PVM:</td>
        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getBaseSubtotal() + $cashOnDeliveryPayment + $invoice->getShippingAmount(), true, false) . '</td>
    </tr>
    <tr>
        <td style="width: 90%; text-align: right;">PVM 21%:</td>
        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getBaseTaxAmount() + $cashOnDeliveryTax, true, false) . '</td>
    </tr>
    <tr>
        <td style="width: 90%; text-align: right;">Suma su PVM:</td>
        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getGrandTotal(), true, false) . '</td>
    </tr>
</table>');

            if (count($invoices) > 1 && count($invoices) != $p) {
                $mpdf->AddPage();
            }
            $p++;
        }

        $mpdf->Output();

        return true;
    }

    protected function singlePdfPage($invoices)
    {
        $this->createPdfPage($invoices, true);
        echo "single";

        return true;
    }

    protected function multiplePdfPages($invoices)
    {
        echo "asd";

        return true;
    }

    protected function createPdfPage($invoices, $singlePage = false)
    {
        if ($singlePage) {
            echo "single page created";
        }
        echo "page created";
    }

    public function getPdf($orderId)
    {
        $mpdf = new Mpdf([
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_top' => 5,
            'margin_bottom' => 25,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);

        $mpdf->SetHTMLFooter('
            <div width="100%" style="text-align:center; width: 100%; font-size: 10px;">
                Trolių namas | Prekės vaikams - Vytauto g. 100 - LT-00133 Palanga - Lietuva<br />
                Norėdami gauti daugiau pagalbos, susisiekite:<br />
                Tel.: +37061316239<br />
                Įmonės kodas: 166929889 PVM mokėtojo kodas: LT669298811
            </div>');

            setlocale(LC_ALL, 'lt_LT.UTF-8');
            $date = strftime('%Y m. %B mėn. %d d.', strtotime($invoice->getCreatedAt()));
            $search = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
            $replace = array('Sausio', 'Vasario', 'Kovo', 'Balandžio', 'Gegužės', 'Birželio', 'Liepos', 'Rugpjūčio', 'Rūgsėjo', 'Spalio', 'Lapkričio', 'Gruodžio');
            $date = str_replace($search, $replace, $date);
            $billingAddress = $invoice->getBillingAddress();
            $mpdf->SetTitle('TN' . $invoice->getIncrementId());
            $cashOnDeliveryPayment = 0;
            $cashOnDeliveryTax = 0;

            $mpdf->WriteHTML('
            <div style="text-align: center; font-size: 14px; margin-bottom: 50px;">
                PVM SĄSKAITA-FAKTŪRA<br />
                Serija Nr.TN' . $invoice->getIncrementId() . '<br />
                ' . $date . '
            </div>
            <table style="width: 100%; font-size: 14px; margin: 0; padding: 0">
                <tr>
                    <td style="width: 50%; display: inline-block; vertical-align:top">
                        PARDAVĖJAS<br /><br />
                        UAB "TROLIŲ NAMAS"<br />
                        Įmonės kodas 166929889<br />
                        PVM kodas LT669298811<br />
                        Adresas: Žemaitijos 20, 89151 Mažeikiai<br />
                        LT667300010129253405 Swedbankas AB
                    </td>
                    <td style="width: 50%; display: inline-block; vertical-align:top">
                        PIRKĖJAS<br /><br />' . $this->helper->getBillingAddress($billingAddress) . '<br />
                    </td>
                </tr>
            </table>
            ');

            $mpdf->WriteHTML('
            <table style="width: 100%; border: 2px solid #000000; margin-top: 20px; font-size: 12px; border-spacing: 0;">
                <tr style="background-color: #DCDCDC; border: 1px solid red;">
                    <td style="width: 50px; text-align: center;">Nr.</td>
                    <td style="text-align: center">Kodas</td>
                    <td style="padding-left: 10px">Pavadinimas</td>
                    <td style="width: 65px; text-align: center">Mato vnt</td>
                    <td style="width: 50px; text-align: center">Kiekis</td>
                    <td style="width: 70px; text-align: center">Kaina su PVM.</td>
                    <td style="width: 70px; text-align: center">Kaina be PVM.</td>
                    <td style="width: 70px; text-align: center">Suma be PVM Eur.</td>
                </tr>
        ');

            $items = $invoice->getOrder()->getAllItems();
            $i = 1;

            foreach ($items as $item) {
                $mpdf->WriteHTML('
                    <tr>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $i++ . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $item->getSku() . '</td>
                        <td style="padding: 10px; border-top: 1px solid #000000;">' . $item->getName() . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . (int)$item->getQtyInvoiced() . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getPriceInclTax(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getPrice(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($item->getRowTotal(), true, false) . '</td>
                    </tr>
                ');
            }

            if ($invoice->getCashOnDeliveryFee() > 0) {
                $mpdf->WriteHTML('
                    <tr>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $i++ . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;"></td>
                        <td style="padding: 10px; border-top: 1px solid #000000;">Atsiskaitymas pristatymo metu</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">1</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee(), true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee() / 1.21, true, false) . '</td>
                        <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getCashOnDeliveryFee() / 1.21, true, false) . '</td>
                    </tr>
                ');

                $cashOnDeliveryPayment = 1.65;
                $cashOnDeliveryTax = 0.35;
            }

            $mpdf->WriteHTML('
<tr>
    <td style="text-align: center; border-top: 1px solid #000000;">' . $i . '</td>
    <td style="text-align: center; border-top: 1px solid #000000;"></td>
    <td style="padding: 10px; border-top: 1px solid #000000;">Pristatymas</td>
    <td style="text-align: center; border-top: 1px solid #000000;">vnt.</td>
    <td style="text-align: center; border-top: 1px solid #000000;">1</td>
    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingInclTax(), true, false) . '</td>
    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingAmount(), true, false) . '</td>
    <td style="text-align: center; border-top: 1px solid #000000;">' . $this->pricingHelper->currency($invoice->getShippingAmount(), true, false) . '</td>
</tr>
');
            $mpdf->WriteHTML('</table>');
            $mpdf->WriteHTML('

<table style="width: 100%; margin-top: 20px; font-size: 12px;">
    <tr>
        <td style="width: 90%; text-align: right;">Suma viso be PVM:</td>
        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getBaseSubtotal() + $cashOnDeliveryPayment + $invoice->getShippingAmount(), true, false) . '</td>
    </tr>
    <tr>
        <td style="width: 90%; text-align: right;">PVM 21%:</td>
        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getBaseTaxAmount() + $cashOnDeliveryTax, true, false) . '</td>
    </tr>
    <tr>
        <td style="width: 90%; text-align: right;">Suma su PVM:</td>
        <td style="text-align: right;">' . $this->pricingHelper->currency($invoice->getGrandTotal(), true, false) . '</td>
    </tr>
</table>');

        $mpdf->Output();

        return true;
    }

    public function getCountryName($countryId): string
    {
        $countryName = '';
        $country = $this->countryFactory->create()->loadByCode($countryId);
        if ($country) {
            $countryName = $country->getName();
        }
        return $countryName;
    }
}
