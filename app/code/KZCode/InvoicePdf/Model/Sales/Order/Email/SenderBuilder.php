<?php
namespace KZCode\InvoicePdf\Model\Sales\Order\Email;

use KZCode\InvoicePdf\Controller\Adminhtml\Invoice\Create;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order;

class SenderBuilder extends \Magento\Sales\Model\Order\Email\SenderBuilder
{
    /**
     * @var \KZCode\InvoicePdf\Model\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    public function __construct(
        Template $templateContainer,
        IdentityInterface $identityContainer,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        Order $order,
        Create $create
    ) {
        $this->templateContainer = $templateContainer;
        $this->identityContainer = $identityContainer;
        $this->transportBuilder = $transportBuilder;
        $this->order = $order;
        $this->invoice = $create;
    }

    /**
     * Configure email template
     *
     * @return void
     */
    protected function configureEmailTemplate()
    {
        $this->transportBuilder->setTemplateIdentifier($this->templateContainer->getTemplateId());
        if ($this->identityContainer  instanceof \Magento\Sales\Model\Order\Email\Container\ShipmentIdentity) {
            $vars = $this->templateContainer->getTemplateVars();
            $order = $vars['order'];
            foreach ($order->getInvoiceCollection() as $invoice) {
                $invoiceId = $invoice->getIncrementId();
            }
            if (isset($invoiceId)) {
                $pdf = $this->invoice->createInvoice($order, 'S');
                $this->transportBuilder->addAttachment($pdf, 'TN' . $invoiceId . '.pdf', 'application/pdf');
            }
        }
        $this->transportBuilder->setTemplateOptions($this->templateContainer->getTemplateOptions());
        $this->transportBuilder->setTemplateVars($this->templateContainer->getTemplateVars());
        $this->transportBuilder->setFromByScope(
            $this->identityContainer->getEmailIdentity(),
            $this->identityContainer->getStore()->getId()
        );
    }
}
