<?php

namespace KZCode\InvoicePdf\Plugin;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Block\Adminhtml\Order\View;
use Magento\Sales\Model\Order;

class PluginBtnOrderView
{
    protected $object_manager;
    protected $backendUrl;

    public function __construct(
        ObjectManagerInterface $om,
        UrlInterface $backendUrl
    ) {
        $this->object_manager = $om;
        $this->backendUrl = $backendUrl;
    }

    public function beforeSetLayout(View $subject)
    {
        if ($subject->getOrder()->getState() == Order::STATE_CANCELED) {
            return null;
        }

        $sendOrder = $this->backendUrl
            ->getUrl('invoicepdf_printbutton/order/index/order_id/'.$subject->getOrderId());
        $subject->addButton(
            'tninvoice',
            [
                'label' => __('Invoice'),
                'onclick' => "setLocation('" . $sendOrder. "')",
                'class' => 'ship',
                'sort_order' => 10
            ]
        );

        return null;
    }

}
