<?php
/**
 * Copyright © 2015 Emizentech. All rights reserved.
 */

namespace KZCode\NewProductsPage\Model\ResourceModel;

class Items extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('emizentech_shopbybrand_items', 'id');
    }
}
