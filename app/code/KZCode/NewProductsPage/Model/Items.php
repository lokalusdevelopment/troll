<?php
/**
 * Copyright © 2015 Emizentech. All rights reserved.
 */

namespace KZCode\NewProductsPage\Model;

class Items extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('KZCode\NewProductsPage\Model\ResourceModel\Items');
    }
}
